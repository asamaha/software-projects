# Software Projects

Here is a curated index of my past projects.

#### Data Collection / Instant Data
Instant Data is a PC-based data collection and analysis suite. It is old and no longer maintained, but the software is still available here free of charge.
* [Instant Data PC](/Resources/InstantData/instantdatapc1.4b.zip) - Data collection app for PCs.
* [Instant Analyzer](/Resources/InstantData/InstantAnalyzer3.6beta.zip) - Session summary and analysis software.
* [Instant Rely](/Resources/InstantData/Instantrely0.5.zip) - Provides various reliability calculations on Instant Data session files.
* [Instant Data .NET](/Resources/InstantData/2010.002instantdatanet.zip) - Data collection app for .NET-based handheld devices.
  * [2008.001_InstantData.exe](/Resources/InstantData/2008.001_InstantData.exe) - A stand-alone executable for .Net-based devices.

#### Reinforcement Schedules
* Observing Response Procedure - [Live Demo](http://jstream.github.io/), [Source Code](https://gitlab.com/asamaha/MultConcurrentVI) - Observing response procedures are useful in the study of conditioned reinforcement because they allow for rates of conditioned and unconditioned reinforcement to be disassociated unlike in chain schedules, in which rates of conditioned and unconditioned reinforcement covary. However, such procedures are difficult to implement in clinical settings because they require the experimenter maintain several separate timers simultaneously (e.g., transitions between components, the duration of Sd presentation, reinforcer schedules, reinforcer access time, etc). This web-app does that.  
